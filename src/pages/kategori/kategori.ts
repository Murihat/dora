import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { MasterRestProvider } from '../../providers/master-rest/master-rest';

/**
 * Generated class for the KategoriPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-kategori',
  templateUrl: 'kategori.html',
})
export class KategoriPage {

  category_menu:any;

  constructor(public navCtrl: NavController, 
              public viewCtrl: ViewController,
              public navParams: NavParams,
              private masterRest: MasterRestProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KategoriPage');
    this.firstload();
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }

  itemSelected(item){
    this.viewCtrl.dismiss(item);
  }


  firstload(){
    this.masterRest.getKategori().then((res) => {
      let result:any = res;

      if(result.status == 1){
        this.category_menu = result.data;
      }else{

      }
      
      console.log(result);
    })
  }

}
