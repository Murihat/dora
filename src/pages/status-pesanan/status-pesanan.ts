import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { MasterRestProvider } from '../../providers/master-rest/master-rest';
import { UserStorageProvider } from '../../providers/user-storage/user-storage';

/**
 * Generated class for the StatusPesananPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-status-pesanan',
  templateUrl: 'status-pesanan.html',
})
export class StatusPesananPage {

  userLogin:any;
  dataPesanan:any;
  loading:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public toastCtrl: ToastController,
              public alertCtrl: AlertController,
              private userStorage: UserStorageProvider,
              private masterRest: MasterRestProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatusPesananPage');
    this.getUserLogin();
  }

  getUserLogin(){
    this.userStorage.getUserLogin().then((res) => {
      let result:any = res;
      this.userLogin = result;
      // console.log(this.userLogin);
      this.firstload();
    })
  }

  firstload(){
    this.showLoading();
    this.userLogin.id_member = this.userLogin.id;
    this.masterRest.statusPesanan(this.userLogin).then((res) => {
      let result:any = res;
      if(result.status == 1){
        this.dataPesanan = result.data;
        console.log(result);
        this.hideLoading();
      }else{
        this.hideLoading();
        let pesan:any = { title: "Server Busy!", msg: "Failed load to server!" }
        this.showAlert(pesan);
      }
    }).catch(err => {
      this.hideLoading();
      let pesan:any = { title: "Server Error!", msg: "waiting for development fix this!" }
        this.showAlert(pesan);
    })
  }

  showAlert(data) {
    const alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.msg,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to close apps",
      duration: 3000,
      position: "bottom",
      cssClass: "toast-exit"
    });
    toast.present();
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading(){
    this.loading.dismiss();
  }
}
