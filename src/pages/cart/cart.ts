import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, App } from 'ionic-angular';
import { MasterRestProvider } from '../../providers/master-rest/master-rest';
import { UserStorageProvider } from '../../providers/user-storage/user-storage';
import { LoginPage } from '../login/login';
import { CheckoutPage } from '../checkout/checkout';

/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  userLogin:any;
  dataCart:any;
  loading:any;
  totalHarga:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              private app: App,
              private userStorage: UserStorageProvider,
              private masterRest: MasterRestProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
    this.getUserLogin();
  }


  getUserLogin(){
    this.userStorage.getUserLogin().then((res) => {
      let result: any = res;
      if(result){
        this.userLogin = result;
        this.firstload();
      }else{
        this.app.getRootNav().setRoot(LoginPage);
      }
      
      console.log(result);
    })
  }


  firstload(){
    this.showLoading();
    this.userLogin.id_member = this.userLogin.id;
    this.masterRest.memberCart(this.userLogin).then((res) => {
      let result: any = res;
      if(result.status == 1){
        this.dataCart = result.data;
        this.totalHarga = 0;
        this.dataCart.forEach((value,index) => {
          this.totalHarga += parseInt(value.total);
        })
        console.log(this.totalHarga);
        this.hideLoading()
      }else{
        this.hideLoading()
        let pesan:any = { title: "Oooppss Sorry!", msg: "No Have Data Cart!" }
        this.showAlert(pesan);
        this.navCtrl.pop();
      }
      console.log(result);
    }).catch(err => {
      this.hideLoading();
      let pesan:any = { title: "Server Busy!", msg: "Please Try Again Later..." }
      this.showAlert(pesan);
    })
  }

  deleteCart(data){
    this.masterRest.deleteCart(data).then((res) => {
      let result:any = res;
      if(result.status == 1){
        this.getUserLogin();
      }else{
        let pesan:any = { title: "Oooppss Sorry!", msg: "Failed to delete this product" }
        this.showAlert(pesan);
      }
      console.log(result);
    })
  }

  qty(x, item){
    console.log(item);
    if(x == 1){
      item.qty++;
      // item.total_harga_beli = (item.harga*item.qty);
      item.total = (item.harga*item.qty);
      this.totalHarga = 0;
      this.dataCart.forEach((value,index) => {
        this.totalHarga += parseInt(value.total);
      })
    }else{
      if(item.qty > 1){
        item.qty--;
        // item.total_harga_beli = (item.harga*item.qty);
        item.total = (item.harga*item.qty);
        this.totalHarga = 0;
        this.dataCart.forEach((value,index) => {
          this.totalHarga += parseInt(value.total);
        })
      }
    }
  }

  buyNow(){
    console.log(this.dataCart);
    this.navCtrl.push(CheckoutPage, {params: this.dataCart});
    // let pesan:any = { title: "Oooppss Sorry!", msg: "Beluman sabar yaaaa..." }
    // this.showAlert(pesan);
  }


  showAlert(data) {
    const alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.msg,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to close apps",
      duration: 3000,
      position: "bottom",
      cssClass: "toast-exit"
    });
    toast.present();
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading(){
    this.loading.dismiss();
  }

}
