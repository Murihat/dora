import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { UserStorageProvider } from '../../providers/user-storage/user-storage';
import { MasterRestProvider } from '../../providers/master-rest/master-rest';
import { HomePage } from '../home/home';

/**
 * Generated class for the PayNowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-pay-now',
  templateUrl: 'pay-now.html',
})
export class PayNowPage {

  data:any;
  totalHarga:any;
  loading:any;
  metode:any;
  userLogin:any = {}

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public navParams: NavParams,
              private userStorage: UserStorageProvider,
              private masterRest: MasterRestProvider) {

              let params: any = this.navParams.get("params");
              let bank:any = this.navParams.get("bank");
              this.metode = bank;
              this.data = params;
              this.totalHarga = 0;
              this.data.forEach((value,index) => {
                this.totalHarga += parseInt(value.total);
                value.metode_pembayaran = this.metode;
              })
              console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PayNowPage');
    this.firstload();
  }

  firstload(){
    this.userStorage.getUserLogin().then((res) => {
      let result:any = res;
      this.userLogin = result;
    })
  }

  paynow(){
    this.showLoading();
    this.data.forEach((value,index) => {
      value.addressShipping = this.userLogin.alamat;
    })

    let alert = this.alertCtrl.create({
      title: 'Anda yakin atas data pembelian tersebut?',
      buttons: [
        {
          text: 'No',
          role: 'no',
          handler: data => {
            console.log("nothing!");
            this.hideLoading();
          }
        },
        {
          text: 'Yes',
          role: 'yes',
          handler: data => {
            console.log(this.data);
            this.masterRest.completePembayaran(this.data).then((res) => {
              let result:any = res;
              console.log(result);
              if(result.status == 1){
                this.hideLoading();
                let pesan:any = { title: "Thanks You!", msg: "Successfully to buy product..." }
                this.showAlert(pesan);
                this.navCtrl.setRoot(HomePage);
              }else{
                this.hideLoading();
                let pesan:any = { title: "Oooppss Sory!", msg: "Failed to buy product..." }
                this.showAlert(pesan);
              }
            }).catch(err => {
              this.hideLoading();
              let pesan:any = { title: "Oooppss Sory!", msg: "Server ERROR!" }
              this.showAlert(pesan);
            })
          }
        }
      ]
    });
    alert.present();
  }

  addAddress(){
    let pesan:any = { title: "Warning!", msg: "Silahkan masukan alamat anda di my profile..." }
    this.showAlert(pesan);
  }


  showAlert(data) {
    const alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.msg,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to close apps",
      duration: 3000,
      position: "bottom",
      cssClass: "toast-exit"
    });
    toast.present();
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading(){
    this.loading.dismiss();
  }


}
