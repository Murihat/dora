import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, LoadingController, ModalController, Modal } from 'ionic-angular';
import { MasterRestProvider } from '../../providers/master-rest/master-rest';
import { DetailPage } from '../detail/detail';
import { KategoriPage } from '../kategori/kategori';
import { CartPage } from '../cart/cart';
import { UserStorageProvider } from '../../providers/user-storage/user-storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  dataProduk:any;
  loading:any;
  badgeCart:any;
  userLogin:any;

  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              public modalCtrl: ModalController,
              private userStorage: UserStorageProvider,
              private masterRest: MasterRestProvider) {

  }

  ionViewDidLoad() {
    console.log("home pages");
    this.firstload();
  }

  ionViewWillEnter(){
    this.badge();
  }

  doRefresh(refresher) {
    setTimeout(() => {
      this.firstload();
      refresher.complete();
    });
  }

  firstload(){
    this.showLoading();
    this.badge();
    let filter:any = {category_id: null}
    this.masterRest.getProduk(filter).then((res) => {
      let result:any = res;
      console.log(result)
      if(result.status == 1){
        this.dataProduk = result.data;
        this.hideLoading();
      }else{
        this.hideLoading();
        let pesan:any = { title: "Oooppss Sorry!", msg: "Cannot Load This Server..." }
        this.showAlert(pesan);
      }
    }).catch(err => {
      this.hideLoading();
      let pesan:any = { title: "Server Busy!", msg: "Please Try Again Later..." }
      this.showAlert(pesan);
    })
  }

  detailProduk(data){
    this.navCtrl.push(DetailPage,{params:data});
  }

  filterCategory(data){
    let filter:any = {category_id: data.id}
    console.log(data)
    this.showLoading();
    this.masterRest.getProduk(filter).then((res) => {
      let result:any = res;
      console.log(result)
      if(result.status == 1){
        this.dataProduk = result.data;
        this.hideLoading();
      }else{
        this.hideLoading();
        let pesan:any = { title: "Oooppss Sorry!", msg: "Cannot Load This Server..." }
        this.showAlert(pesan);
      }
    }).catch(err => {
      this.hideLoading();
      let pesan:any = { title: "Server Busy!", msg: "Please Try Again Later..." }
      this.showAlert(pesan);
    })
  }

  filter(){
    const myModal: Modal = this.modalCtrl.create(KategoriPage);
    myModal.present();

    myModal.onWillDismiss((data) => {
      console.log("bawah");
      if(data){
        this.filterCategory(data);
      }else{
        this.firstload();
      }
      
    })

  }

  badge(){
    this.userStorage.getUserLogin().then((res) => {
      let result: any = res;
      if(result){
        this.userLogin = result;
        this.userLogin.id_member = this.userLogin.id;
        this.masterRest.badge(this.userLogin).then((res) => {
          let result: any = res;
          if(result.status == 1){
            this.badgeCart = result.data;
            console.log(result)
          }
        })
        
      }
    })
    
  }

  cart(){
    this.navCtrl.push(CartPage);
  }

  showAlert(data) {
    const alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.msg,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to close apps",
      duration: 3000,
      position: "bottom",
      cssClass: "toast-exit"
    });
    toast.present();
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading(){
    this.loading.dismiss();
  }

}
