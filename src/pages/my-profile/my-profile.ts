import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { UserStorageProvider } from '../../providers/user-storage/user-storage';
import { MasterRestProvider } from '../../providers/master-rest/master-rest';
import { HomePage } from '../home/home';

/**
 * Generated class for the MyProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-my-profile',
  templateUrl: 'my-profile.html',
})
export class MyProfilePage {

  data:any = {}
  loading:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public event: Events,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              private userStorage: UserStorageProvider,
              private masterRest: MasterRestProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyProfilePage');
    this.firstload();
  }


  firstload(){
    this.userStorage.getUserLogin().then((res) => {
      let result:any = res;
      this.data = result;
      console.log(this.data)
    }).then( () => { 
      this.event.subscribe("login", (userLogin) => {
        this.data = userLogin;
      });
      this.event.subscribe("editProfile", (userLogin) => {
        this.data = userLogin;
      });
    }) 
  }


  simpan(){
    this.showLoading();
    this.masterRest.editMember(this.data).then((res) => {
      let result: any = res;
      console.log(result);
      if(result.status == 1){
        this.hideLoading();
        this.userStorage.setUserLogin(result.data);
        this.event.publish("editProfile", result.data);
        let pesan:any = { title: "Thanks You!", msg: result.message }
        this.showAlert(pesan);
        this.navCtrl.setRoot(HomePage);
      }else{
        let pesan:any = { title: "Oooppss Sorry!", msg: result.message }
        this.showAlert(pesan);
        this.hideLoading();
      }
    }).catch(err => {
      this.hideLoading();
      let pesan:any = { title: "Server Busy!", msg: "Failed load to server!" }
      this.showAlert(pesan);
    })
  }


  showAlert(data) {
    const alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.msg,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to close apps",
      duration: 3000,
      position: "bottom",
      cssClass: "toast-exit"
    });
    toast.present();
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading(){
    this.loading.dismiss();
  }

}
