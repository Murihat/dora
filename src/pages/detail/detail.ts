import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController, App } from 'ionic-angular';
import { MasterRestProvider } from '../../providers/master-rest/master-rest';
import { UserStorageProvider } from '../../providers/user-storage/user-storage';
import { CartPage } from '../cart/cart';
import { LoginPage } from '../login/login';

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {

  dataDetail:any;
  userLogin:any;
  pesan:any = {ukuran: '', quantity: ''}
  loading:any;
  badgeCart:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              private app: App,
              private userStorage: UserStorageProvider,
              private masterRest: MasterRestProvider) {

            let params:any = this.navParams.get("params");
            this.dataDetail = params;
            console.log(this.dataDetail);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
    this.getUserLogin();
  }

  ionViewWillEnter(){
    this.badge();
  }

  getUserLogin(){
    this.userStorage.getUserLogin().then((res) => {
      let result: any = res;
      if(result){
        this.userLogin = result;
      }
      
      console.log(result);
    })
  }

  addToCart(){
    this.showLoading();
    if(this.userLogin){
    this.dataDetail.id_member = this.userLogin.id;
    this.dataDetail.id_produk = this.dataDetail.id;
    this.dataDetail.ukuran = this.pesan.ukuran;
    this.dataDetail.qty = (this.pesan.quantity == '') ? 1 : this.pesan.quantity;

      this.masterRest.addToCart(this.dataDetail).then((res) => {
        let result:any = res;
        console.log(result);
        if(result.status == 1){

          this.hideLoading()
          this.badge();
          let pesan:any = { title: "Thanks You!", msg: "Success add to cart.." }
          this.showAlert(pesan);
         
        }else{
          this.hideLoading()
          let pesan:any = { title: "Oooppss Sorry!", msg: "Failed to add cart..." }
          this.showAlert(pesan);
        }
      }).catch(err => {
        this.hideLoading();
        let pesan:any = { title: "Server Busy!", msg: "Please Try Again Later..." }
        this.showAlert(pesan);
      })
    }else{
      this.hideLoading();
      this.app.getRootNav().setRoot(LoginPage);
    }
    
  }

  badge(){
    this.userLogin.id_member = this.userLogin.id;
    this.masterRest.badge(this.userLogin).then((res) => {
      let result: any = res;
      if(result.status == 1){
        this.badgeCart = result.data;
        console.log(result)
      }
    })
  }

  cart(){
    this.navCtrl.push(CartPage);
  }

  showAlert(data) {
    const alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.msg,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to close apps",
      duration: 3000,
      position: "bottom",
      cssClass: "toast-exit"
    });
    toast.present();
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading(){
    this.loading.dismiss();
  }

}
