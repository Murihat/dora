import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayNowPage } from '../pay-now/pay-now';

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {

  data:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams) {

                let params:any = this.navParams.get("params");
                this.data = params;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }


  payNow(bank){
    console.log(bank)
    this.navCtrl.push(PayNowPage,{bank: bank, params:this.data})
  }
}
