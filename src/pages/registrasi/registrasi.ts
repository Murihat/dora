import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { MasterRestProvider } from '../../providers/master-rest/master-rest';

/**
 * Generated class for the RegistrasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-registrasi',
  templateUrl: 'registrasi.html',
})
export class RegistrasiPage {

  data:any = {namaPerusahaan: '', namaAnda: '', jabatan: '', noHp: '', alamat: '', email: '', password: ''}
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  loading:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              private masterRest: MasterRestProvider,
              ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrasiPage');
  }

  register(){ 
    this.showLoading();
    if(this.data.namaPerusahaan == '' || this.data.namaAnda == '' || this.data.jabatan == '' || this.data.noHp == '' || this.data.alamat == '' || this.data.email == '' || this.data.password == ''){
      this.hideLoading();
      let pesan:any = { title: "Oooppss Maaf!", msg: "Silahkan Lengkapi Data Registrasi..." }
      this.showAlert(pesan);
    }else{
      console.log(this.data);
      this.masterRest.registrasi(this.data).then((res) => {
        let result:any = res;
        if(result.status == 1){
          this.hideLoading();
          let pesan:any = { title: "Terimakasih!", msg: "Registrasi Berhasil, Silahkan Login..." }
          this.showAlert(pesan);
          this.navCtrl.pop();
        }else{
          this.hideLoading();
          let pesan:any = { title: "Oooppss Maaf!", msg: "Registrasi Gagal, Silahkan Coba Beberapa Saat Lagi..." }
          this.showAlert(pesan);
        }
      }).catch(err => {
        this.hideLoading();
        let pesan:any = { title: "Oooppss Maaf!", msg: "Gagal Menghubungkan Keserver..." }
        this.showAlert(pesan);
      })
    }
  }


  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }


  showAlert(data) {
    const alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.msg,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to close apps",
      duration: 3000,
      position: "bottom",
      cssClass: "toast-exit"
    });
    toast.present();
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading(){
    this.loading.dismiss();
  }

}
