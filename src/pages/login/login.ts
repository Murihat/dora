import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { MasterRestProvider } from '../../providers/master-rest/master-rest';
import { UserStorageProvider } from '../../providers/user-storage/user-storage';
import { RegistrasiPage } from '../registrasi/registrasi';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  backgroundImage="assets/imgs/bg-login.jpg";
  loading:any;
  userLogin:any = {email: '', password: ''}

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public navParams: NavParams,
              private event: Events,
              private userStorage: UserStorageProvider,
              private masterRest: MasterRestProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.cekLogin();
  }

  cekLogin(){
    this.userStorage.getUserLogin().then((res) => {
      let result:any = res;
      if(result){
        this.navCtrl.setRoot(HomePage);
      }
    })
  }

  backBtn(){
    this.navCtrl.setRoot(HomePage);
  }

  login(){
    this.showLoading()
    if(this.userLogin.email == '' || this.userLogin.pasword == ''){
      this.hideLoading();
      let pesan:any = { title: "Oooppss Sorry!", msg: "Please Enter Your Email And Password!" }
      this.showAlert(pesan);
    }else{
      this.masterRest.login(this.userLogin).then((res) => {
        let result:any = res;
        if(result.status == 1){
          this.userLogin = result.data;
          this.navCtrl.setRoot(HomePage);
          this.event.publish("login", this.userLogin);
          this.userStorage.setUserLogin(this.userLogin);
          this.hideLoading();
        }else{
          this.hideLoading();
          let pesan:any = { title: "Oooppss Sorry!", msg: "Please Check Your Email And Password!" }
          this.showAlert(pesan);
        }
      }).catch(err => {
        this.hideLoading();
        let pesan:any = { title: "Server Busy!", msg: "Failed load to server!" }
        this.showAlert(pesan);
      })
    }
  
  }

  register(){
    // let pesan:any = { title: "Oooppss Sorry!", msg: "On progress development..." }
    // this.showAlert(pesan);
    this.navCtrl.push(RegistrasiPage);
  }

  showAlert(data) {
    const alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.msg,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to close apps",
      duration: 3000,
      position: "bottom",
      cssClass: "toast-exit"
    });
    toast.present();
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading(){
    this.loading.dismiss();
  }
}
