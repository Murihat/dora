import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, App, MenuController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { UserStorageProvider } from '../providers/user-storage/user-storage';
import { MyProfilePage } from '../pages/my-profile/my-profile';
import { StatusPesananPage } from '../pages/status-pesanan/status-pesanan';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild('content') Nav:NavController;

  rootPage:any = HomePage;
  user:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, 
              private app: App, public menuCtrl: MenuController, private event: Events, private userStorage: UserStorageProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      statusBar.styleLightContent();
      splashScreen.hide();
    });

    this.getUserLogin();
    this.event.subscribe("logout", () => {
      this.user = false;
    });
  }

  getUserLogin(){
    this.userStorage.getUserLogin().then((res) => {
      let result:any = res;
      if(result){
        this.user = result;
      }
    }).then( () => { 
      this.event.subscribe("login", (userLogin) => {
        this.user = userLogin;
      });
      this.event.subscribe("editProfile", (userLogin) => {
        this.user = userLogin;
      });
    }) 
  }

  openPage(err){
    console.log(err)
  }

  login(){
    this.menuCtrl.close();
    this.app.getRootNav().setRoot(LoginPage);
  }

  myprofile(){
    this.menuCtrl.close();
    this.Nav.push(MyProfilePage);
  }

  statusPesanan(){
    this.menuCtrl.close();
    this.Nav.push(StatusPesananPage);
  }

  logout(){
    this.menuCtrl.close();
    this.event.publish("logout");
    this.userStorage.removeUserLogin();
    this.Nav.setRoot(this.rootPage);
  }
}
