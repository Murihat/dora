import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';


import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MasterRestProvider } from '../providers/master-rest/master-rest';
import { DetailPage } from '../pages/detail/detail';
import { LoginPage } from '../pages/login/login';
import { UserStorageProvider } from '../providers/user-storage/user-storage';
import { IonicStorageModule } from '@ionic/storage';
import { RegistrasiPage } from '../pages/registrasi/registrasi';
import { KategoriPage } from '../pages/kategori/kategori';
import { CartPage } from '../pages/cart/cart';
import { CheckoutPage } from '../pages/checkout/checkout';
import { MyProfilePage } from '../pages/my-profile/my-profile';
import { PayNowPage } from '../pages/pay-now/pay-now';
import { StatusPesananPage } from '../pages/status-pesanan/status-pesanan';


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    DetailPage,
    RegistrasiPage,
    KategoriPage,
    CartPage,
    LoginPage,
    MyProfilePage,
    CheckoutPage,
    PayNowPage,
    StatusPesananPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp,  {scrollPadding: false,
      scrollAssist: false, 
      autoFocusAssist: false})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    DetailPage,
    RegistrasiPage,
    KategoriPage,
    CartPage,
    LoginPage,
    MyProfilePage,
    CheckoutPage,
    PayNowPage,
    StatusPesananPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MasterRestProvider,
    UserStorageProvider
  ]
})
export class AppModule {}
