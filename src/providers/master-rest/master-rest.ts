import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the MasterRestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MasterRestProvider {

  apiUrl :string =  "http://murihat.com/dora/dashboard/api";

  constructor(public http: HttpClient) {
    console.log('Hello MasterRestProvider Provider');
  }

  setHeader(){
    let headers = new HttpHeaders({
       'Content-Type':'application/json',
       'Authorization': ''
    })
    return headers;
  }

  login(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/login', JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  getProduk(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/getProduk',  JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  getKategori(){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.get(this.apiUrl+'/getCategory')
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }


  registrasi(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/register', JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }


  addToCart(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/addToCart', JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }


  memberCart(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/memberCart', JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteCart(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/deleteCart', JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  badge(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/countBadgeCart', JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  editMember(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/editMember', JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  completePembayaran(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/completePembayaran', JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  statusPesanan(data){
    const _setHeader   = {
      headers:this.setHeader()
    };
    
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/statusPesan', JSON.stringify(data))
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }


}
