import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

/*
  Generated class for the UserStorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserStorageProvider {

  constructor(private storage: Storage) {
    console.log('Hello UserStorageProvider Provider');
  }


  setUserLogin(data){
    return new Promise((resolve, reject)=> {
      this.storage.set('userLogin', data).then(res=>resolve(res))
      .catch(err=>reject(err));
    })
  }

  getUserLogin(){
    return new Promise((resolve, reject)=> {
      this.storage.get('userLogin').then(res=>resolve(res))
      .catch(err=>reject(err));
    })
  }

  
  removeUserLogin(){
    return new Promise((resolve, reject)=> {
      this.storage.remove('userLogin').then(res=>resolve(res))
      .catch(err=>reject(err));
    })
  }

}
